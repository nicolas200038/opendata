<?php

$number = 0;
$file = "https://data.enseignementsup-recherche.gouv.fr/api/records/1.0/search/?dataset=fr-esr-principaux-diplomes-et-formations-prepares-etablissements-publics&rows=100&sort=-rentree_lib&facet=rentree_lib&facet=etablissement_type2&facet=etablissement_type_lib&facet=etablissement&facet=etablissement_lib&facet=champ_statistique&facet=dn_de_lib&facet=cursus_lmd_lib&facet=diplome_rgp&facet=diplome_lib&facet=typ_diplome_lib&facet=diplom&facet=niveau_lib&facet=disciplines_selection&facet=gd_disciscipline_lib&facet=discipline_lib&facet=sect_disciplinaire_lib&facet=spec_dut_lib&facet=localisation_ins&facet=com_etab&facet=com_etab_lib&facet=uucr_etab&facet=uucr_etab_lib&facet=dep_etab&facet=dep_etab_lib&facet=aca_etab&facet=aca_etab_lib&facet=reg_etab&facet=reg_etab_lib&facet=com_ins&facet=com_ins_lib&facet=uucr_ins&facet=dep_ins&facet=dep_ins_lib&facet=aca_ins&facet=aca_ins_lib&facet=reg_ins&facet=reg_ins_lib"; 

// Définition de la demande pour l'API
if(isset($_POST['region'])) {
	if($_POST['region']=="etr") {
		$file.="&refine.localisation_ins=%C3%89tranger"; 
	}
	if($_POST['region']=="idf") {
		$file.="&refine.localisation_ins=%C3%8Ele-de-France"; 
	}
	if($_POST['region']=="ara") {
		$file.="&refine.localisation_ins=Auvergne-Rh%C3%B4ne-Alpes"; 
	}
	if($_POST['region']=="ge") {
		$file.="&refine.localisation_ins=Grand+Est";
	}
	if($_POST['region']=="na") {
		$file.="&refine.localisation_ins=Nouvelle-Aquitaine";
	}
	if($_POST['region']=="hdf") {
		$file.="&refine.localisation_ins=Hauts-de-France ";
	}
	
}

$json = file_get_contents($file); // appel à l'API
$donnees = json_decode($json);
$nhits = $donnees->nhits;




echo "<p style='font-size: 26px;font-weight: bold;padding-bottom: 15px;'>Résultats de la recherche</p>";
echo "<table>";
echo "<tr>
			<th>Etablissement</th>
			<th>Disciplines</th>
			<th>Type de diplome</th>
			<th>Région</th>
		  </tr>";

while($number<$nhits)
{
	$etablissement = $donnees->records[$number]->fields->etablissement_lib;
	$themes = $donnees->records[$number]->fields->gd_disciscipline_lib;
	$diplome = $donnees->records[$number]->fields->typ_diplome_lib;
	$region = $donnees->records[$number]->fields->reg_ins_lib;

	echo "<tr>
			<td>".$etablissement."</td>
			<td>".$themes."</td>
			<td>".$diplome."</td>
			<td>".$region."</td>
		  </tr>";
	$number++;
}

echo "</table>";



?>