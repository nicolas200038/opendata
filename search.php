<html lang="fr">
<head>
  <meta charset="utf-8">
  <link rel="stylesheet" href="style.css" />
  <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
  <link rel="stylesheet" href="https://unpkg.com/leaflet@1.6.0/dist/leaflet.css"
   integrity="sha512-xwE/Az9zrjBIphAcBb3F6JVqxf46+CDLwfLMHloNu6KEQCAWi6HcDUbeOfBIptF7tcCzusKFjFw2yuvEpDL9wQ=="
   crossorigin=""/>
   <script src="https://unpkg.com/leaflet@1.6.0/dist/leaflet.js"
   integrity="sha512-gZwIG9x3wUXg2hdXF6+rVkLF/0Vi9U8D2Ntg4Ga5I5BZpVkVxlJWbSQtXPSiUTtC0TjtGOmxa1AJPuV0CPthew=="
   crossorigin=""></script>

  <title>Votre recherche</title>
</head>

<body>
	<div class="conteneur">
		<div class="navbar">
			<a class="home" href="index.php">Accueil</a>
  			<a href="#bac">Après le bac</a>
  			<a href="#parcours">Après un parcours universitaire</a>
  			<a href="#contact">Contact</a>
		</div>
	</div>


	<div class="containerLine">
	<div class="spaceTopBot">

		<div class="searchSpace">
			<div class="titleFilter">
				<div class="textFilter">
					<ul>
						<li class="textFilterLi">Votre recherche</li>
					</ul>

				</div>	

			</div>

			<div class="researchText">
					<input type="text" name="personalresearch">
			</div>

			<br>
			
			<div class="middleText">
					<input type="button" name="valider" value="Rechercher">
			</div>

		</div>
	</div>


	<div class="spaceTopBot">
		<div class="filter">
			<div class="titleFilter">
				<div class="textFilter">
					<ul>
						<li class="textFilterLi">Filtres</li>
						<a href=""><li class="textFilterLi2">Tout effacer</li></a>
					</ul>
				</div>
			</div>

			<form action="search.php" method="post">
			<div class="textFilter2">
				<p style="color: white;">Par département</p>
				<hr>
			</div>
				<div class="middleText">
				<select id="region" name="region">

					<option value="idf">Ile-de-France</option>
					<option value="ara">Auvergne-Rhone-Alpes</option>
					<option value="ge">Grand Est</option>
					<option value="na">Nouvelle-Aquitaine</option>
					<option value="hdf">Hauts-de-France</option>
					<option value="occ">Occitanie</option>
					<option value="nor">Normandie</option>
					<option value="paca">Provence-Alpes-Cote d'Azur</option>
					<option value="bre">Bretagne</option>
					<option value="bfc">Bourgogne-Franche-Comté</option>
					<option value="pll">Pays de la Loire</option>
					<option value="cvl">Centre-Val de Loire</option>
					<option value="cor">Corse</option>
					<option value="reu">La Réunion</option>
					<option value="guy">Guyane</option>
					<option value="gua">Guadeloupe</option>
					<option value="mar">Martinique</option>
					<option value="com">Collectivités d'outre-mer</option>
					<option value="etr">Étranger</option>

				</select>
				</div>

				<div class="textFilter2">
					<p style="color: white;">Par domaine</p>
					<hr>
				</div>

				<div class="checkForm">
					<input type="checkbox" value="sciences" name="science">
					<label for="Sciences">Sciences</label>

					<br>

					<input type="checkbox" value="informatique" name="informatic">
					<label for="Informatique">Informatique</label>

					<br>

					<input type="checkbox" value="economie" name="economist">
					<label for="Economie">Economie</label>

					<br>

					<input type="checkbox" value="social" name="social">
					<label for="Social">Social</label>

					<br>

					<input type="checkbox" value="medecine" name="medical">
					<label for="Médecine">Médecine</label>

					<br>

					<input type="checkbox" value="mathematiques" name="mathematics">
					<label for="Mathématiques">Mathématiques</label>

					<br>

					<input type="checkbox" value="langues" name="langues">
					<label for="Langue">Langues</label>

					<br>

					<input type="checkbox" value="lettres" name="lettres">
					<label for="Lettres">Lettres</label>

					<br>

					<input type="checkbox" value="droit" name="droit">
					<label for="Droit">Droit</label>

					<br>

					<input type="checkbox" value="staps" name="staps">
					<label for="Langue">STAPS</label>

					<br>

					<input type="checkbox" value="histoire" name="history">
					<label for="Histoire">Histoire</label>

					<br>

					<input type="checkbox" value="art" name="art">
					<label for="Art">Art</label>

				</div>

				<div class="textFilter2">
					<p style="color: white;">Par diplôme</p>
					<hr>
				</div>

				<div class="checkForm">
				<input type="checkbox" value="doctorat" name="doctorat">
					<label for="Langue">Doctorat</label>
				<br>

				<input type="checkbox" value="habilitation" name="habilitation">
					<label for="Langue">Habilitation</label>

				<br>

				<input type="checkbox" value="dut" name="dut">
					<label for="DUT">DUT</label>

				<br>

				<input type="checkbox" value="bts" name="bst">
					<label for="BTS">BTS</label>

				</div>

				<br>

				<div class="middleText">
					<input type="submit" name="valider" value="Rechercher">
				</div>
			</form>
		</div>
	</div>
</div>

<div class="mapSpace">
		<div id="mapid"></div>
</div>

<?php
	if($_POST['valider'] && (isset($_POST['region']))) {
		echo "<div class='tableau'>";
		include("bases_de_donnees.php");
		echo "</div>";
	}
?>

	<div class="footer">
		<p>Nicolas FERNANDES</p>
	</div>
</div>

</body>

<script>
	var mymap = L.map('mapid').setView([51.505, -0.09], 13);
	L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    maxZoom: 18,
    id: 'mapbox/streets-v11',
    accessToken: 'pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw'
}).addTo(mymap);

</script>

</html>